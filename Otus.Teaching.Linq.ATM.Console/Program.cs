﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using Otus.Teaching.Linq.ATM.Core.Services;
using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            string firsName = "Ann";
            string surName = "Lee";
            string login = "lee";
            string password = "222";

            //TODO: Далее выводим результаты разработанных LINQ запросов

            System.Console.WriteLine();
            // Вывод информации о заданном аккаунте по логину и паролю
            var user = atmManager.ShowAccountInfo(login, password);
            foreach (var u in user)
            {
                System.Console.WriteLine($"Номер : {u.Id} ");
                System.Console.WriteLine($"Имя : {u.FirstName}");
                System.Console.WriteLine($"Фамилия : {u.SurName}");
                System.Console.WriteLine($"Отчество : {u.MiddleName}");
                System.Console.WriteLine($"Телефон : {u.Phone}");
                System.Console.WriteLine($"Паспорт : {u.PassportSeriesAndNumber}");
                System.Console.WriteLine($"Дата регистрации : {u.RegistrationDate}");
                System.Console.WriteLine($"Логин : {u.Login}");
            }

            System.Console.WriteLine();
            // Вывод данных о всех счетах заданного пользователя
            var accounts = atmManager.ShowAccountAll(firsName, surName);
            foreach (var u in accounts)
            {
                System.Console.WriteLine($"Номер : {u.Id} ");
                System.Console.WriteLine($"Дата открытия : {u.OpeningDate}");
                System.Console.WriteLine($"Сумма : {u.CashAll}");
                System.Console.WriteLine($"Юзер ИД : {u.UserId}");
            }

            System.Console.WriteLine();
            // Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            var operationHistory = atmManager.ShowAccountFromHistory(firsName, surName);
            foreach (var u in operationHistory)
            {
                System.Console.WriteLine($"Номер : {u.Id} ");
                System.Console.WriteLine($"Дата операции : {u.OperationDate}");
                switch (u.OperationType) 
                {
                    case Core.Entities.OperationType.InputCash :
                        System.Console.WriteLine($"Тип Операции : Внесение денег");
                        break;
                    case Core.Entities.OperationType.OutputCash :
                            System.Console.WriteLine($"Тип Операции : Снятие Денег");
                            break;
                }
                System.Console.WriteLine($"Сумма операции : {u.CashSum}");
                System.Console.WriteLine($"Акаунт ИД : {u.AccountId}");
            }

            System.Console.WriteLine();
            // Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
            var depositFromUsers = atmManager.ShowDepositFromUser(Core.Entities.OperationType.InputCash);
            foreach(var u in depositFromUsers)
            {
                System.Console.WriteLine($"Номер : {u.Id} ");
                System.Console.WriteLine($"Имя : {u.FirstName}" + $"Фамилия : {u.SurName}");
                System.Console.WriteLine($"Дата операции : {u.OperationDate}");
                System.Console.WriteLine($"Операция : {u.OperationType}");
            }

            System.Console.WriteLine();
            // Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
            var users = atmManager.ShowUserCashIsGreater(5000m);
            foreach (var u in users)
            {
                System.Console.WriteLine($"Номер : {u.Id} ");
                System.Console.WriteLine($"Имя : {u.FirstName}");
                System.Console.WriteLine($"Фамилия : {u.SurName}");
                System.Console.WriteLine($"Отчество : {u.MiddleName}");
                System.Console.WriteLine($"Телефон : {u.Phone}");
                System.Console.WriteLine($"Паспорт : {u.PassportSeriesAndNumber}");
                System.Console.WriteLine($"Дата регистрации : {u.RegistrationDate}");
                System.Console.WriteLine($"Логин : {u.Login}");
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}