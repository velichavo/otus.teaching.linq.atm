﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System.Reflection.Metadata;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }

        public IEnumerable<DepositFromUser> DepositFromUsers { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата

        public IEnumerable<User> ShowAccountInfo(string login, string password)
        {
            return Users.Where(u => u.Login == login && u.Password == password).ToList();
        }

        public IEnumerable<Account> ShowAccountAll(string firstName, string surName)
        {
            var user = Users.Where(u => u.FirstName == firstName && u.SurName == surName).ToList();
            return Accounts.Where(a => a.UserId == user.ElementAt<User>(0).Id).ToList();
        }

        public IEnumerable<OperationsHistory> ShowAccountFromHistory(string firstName, string surName)
        {
            var account = ShowAccountAll(firstName, surName);
            var history = new List<OperationsHistory>();

            history = History.Where(h => account.Where(a => a.Id == h.AccountId).Count() > 0).ToList(); 

            return history;
        }

        public IEnumerable<DepositFromUser> ShowDepositFromUser(OperationType type)
        {
            var users = Users.ToList();
            var account = Accounts.ToList();
            var history = new List<OperationsHistory>();

            history = history.Concat(History.Where(h => h.OperationType == type)).ToList();


            var depositFromUsers = (from h in history
                               join acc in account on h.AccountId equals acc.Id
                               join u in users on acc.UserId equals u.Id
                               select new DepositFromUser
                               {
                                   Id = h.Id,
                                   OperationDate = h.OperationDate,
                                   OperationType = h.OperationType,
                                   CashSum = h.CashSum,
                                   AccountId = h.AccountId,
                                   FirstName = u.FirstName,
                                   SurName = u.SurName

                               }).ToList();


            return depositFromUsers;
        }
        public IEnumerable<User> ShowUserCashIsGreater(decimal sum)
        {
            var users = new List<User>();

            users = Users.Where(u => Accounts.Where(a => a.UserId == u.Id).Where(a => a.CashAll >= sum).Count() > 0).ToList();

            return users;

        }

    }
}