﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class DepositFromUser
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
    }
}
